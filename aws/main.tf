###############################
#        STAGE1               #
###############################

module "vpc" {
    source                              = "modules/vpc"
    name                                = "default"
    environment                         = "staging"

    instance_tenancy                    = "default"
    enable_dns_support                  = "true"
    enable_dns_hostnames                = "true"
    assign_generated_ipv6_cidr_block    = "false"
    enable_classiclink                  = "false"
    vpc_cidr                            = "172.31.0.0/16"
    private_subnet_cidrs                = ["172.31.64.0/24", "172.31.63.0/24"]
    public_subnet_cidrs                 = ["172.31.80.0/24"]
    availability_zones                  = ["us-east-1a", "us-east-1b"]
    allowed_ports                       = []
    enable_internet_gateway             = "true"
    enable_nat_gateway                  = "false"
    single_nat_gateway                  = "true"
    enable_vpn_gateway                  = "false"
    enable_dhcp_options                 = "false"
    enable_eip                          = "false"
}

module "iam" {
    source                          = "modules/iam"
    name                            = "iam-ec2"
    region                          = "us-east-1"
    environment                     = "staging"
    aws_iam_role-principals         = [
        "ec2.amazonaws.com",
    ]

    aws_iam_policy-actions = [
        "cloudwatch:GetMetricStatistics",
        "logs:DescribeLogStreams",
        "logs:GetLogEvents",
        "elasticache:Describe*",
        "rds:Describe*",
        "rds:ListTagsForResource",
        "ec2:DescribeAccountAttributes",
        "ec2:DescribeAvailabilityZones",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeVpcs",
        "ec2:Owner",
    ]
}

###############################
#        STAGE2               #
###############################

module "efs" {
    source          = "modules/efs"
    name            = "web"
    region          = "us-east-1"
    environment     = "staging"
    subnet_ids      = ["${module.vpc.vpc-privatesubnet-ids}"]
    security_groups = ["${module.vpc.security_group_id}"]
}

module "s3" {
    source                              = "modules/s3"
    name                                = "humansonly"
    environment                         = "staging"

    #
    s3_acl          = "log-delivery-write"  #"private"
    force_destroy   = "true"
}

###############################
#        STAGE3               #
###############################

module "ec2" {
    source                              = "modules/ec2"
    name                                = "web"
    region                              = "us-east-1"
    environment                         = "staging"
    ec2_instance_type                   = "t2.micro"
    enable_associate_public_ip_address  = "true"
    disk_size                           = "8"
    tenancy                             = "${module.vpc.instance_tenancy}"
    iam_instance_profile                = "${module.iam.instance_profile_id}"
    subnet_id                           = "${element(module.vpc.vpc-publicsubnet-ids, 0)}"
    #subnet_id                           = "${element(module.vpc.vpc-privatesubnet-ids, 0)}"
    #subnet_id                           = ["${element(module.vpc.vpc-privatesubnet-ids)}"]
    vpc_security_group_ids              = ["${module.vpc.security_group_id}"]
    monitoring                          = "true"
}

###############################
#        STAGE4               #
###############################

module "elb" {
    source                              = "modules/elb"
    name                                = "web"
    region                              = "us-east-1"
    environment                         = "staging"
    security_groups                     = ["${module.vpc.security_group_id}"]
    subnets                             = ["${element(module.vpc.vpc-publicsubnet-ids, 0)}"]

    listener = [
        {
            instance_port     = "80"
            instance_protocol = "HTTP"
            lb_port           = "80"
            lb_protocol       = "HTTP"
        }
    ]

    health_check = [
        {
            target              = "HTTP:80/"
            interval            = 30
            healthy_threshold   = 2
            unhealthy_threshold = 2
            timeout             = 5
        }
    ]

    enable_lb_cookie_stickiness_policy_http  = true
    enable_app_cookie_stickiness_policy_http = "true"
    instances = ["${module.ec2.instance_ids}"]

}

###############################
#        STAGE5               #
###############################

module "rds" {
    source              = "modules/rds"
    name                = "humansonly"
    region              = "us-east-1"
    environment         = "staging"
    subnet_ids          = ["${module.vpc.vpc-privatesubnet-ids}"]
    # Single node(s)
    create_rds_cluster  = false
    engine              = "postgres"
    engine_version      = "9.6"
    instance_class      = "db.t2.micro"
    db_name             = "humansonly"
    db_username         = "root"
    db_password         = "humansonly"

    ## For cluster
    #create_rds_cluster  = false
    ## aurora or aurora-postgresql. !!!!Tested just for aurora!!!!
    #engine              = "aurora"
    #instance_class      = "db.t2.small"

}
