
#resource "ovirt_disk" "my_disk" {
# name = "centos7_disk"
#  size = 10240
#  format = "cow"
#  storage_domain_id = "eba96514-91f6-4d2c-bfa7-ded149890254"
#  sparse = true
#}


resource "ovirt_vm" "Centos7" {
  name = "Centos7_terraform"
  cluster = "Protei4"
  authorized_ssh_key = "${file(pathexpand("~/.ssh/id_rsa.pub"))}"
  cores = "1"
  memory = "8024"
  network_interface {
    label = "eth0"
    boot_proto = "static"
    ip_address = "192.168.111.3"
    gateway = "192.168.0.254"
    subnet_mask = "255.255.128.0"
 }
#   attached_disks = [{
#    disk_id = "${ovirt_disk.my_disk.id}"
#    bootable = "true"
#    interface  = "virtio"
# }]

  template = "Centos7"
  provisioner "remote-exec" {
    connection {
      type  = "ssh"
      user = "root"
      agent = "false"
      private_key = "${file("~/.ssh/id_rsa")}" 

  }
    inline = [
      "echo 'nameserver 192.168.100.143' > /etc/resolv.conf && route add default gw 192.168.0.254 && yum -y install mc"
    ]
 }

}