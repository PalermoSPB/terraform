**PLAYBOOK для создания инфраструктуры с помощью terraform на AWS Free Tier**
----------

## Usage


**STAGE - 1**

`terraform apply -target=module.vpc` # *Create VPC, Create security group, Add security group rules, Add AWS subnets (private),  Add AWS subnets (public), Add AWS internet gateway, Create EIP (Optional), Create NAT, Create DHCP, etc.*

**STAGE - 2**

`terraform apply -target=module.efs` # *Create AWS EFS file system, Create AWS EFS mount target*

`terraform apply -target=module.s3` # *Create S3 bucket*

**STAGE - 3**

`terraform apply -target=module.ec2` # *Define SSH key pair, Create AWS Instance & Provisioning* 

**STAGE - 4**

`terraform apply -target=module.elb` # *Create AWS ELB, ELB attachment*

**STAGE - 5**

`terraform apply -target=module.rds` # *Create AWS RDS instance(s), Create AWS RDS cluster (opt), Create AWS DB subnet group, Create AWS DB parameter group*

--------