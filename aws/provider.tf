terraform {
  required_version = "> 0.9.0"
}
provider "aws" {
    profile = "staging"
    region  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    access_key = "" # AWS IAM key 
    secret_key = "" # AWS IAM secret
}
